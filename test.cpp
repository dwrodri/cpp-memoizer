#include "memoize.hpp"
#include <iostream>
#include <string>
#include <random>
#include <cstdlib>


//Fib is intended to measure the overhead of call stack generation
uint64_t Fib(uint64_t x) {
    if (x == 0) return 0;
    else if (x <= 2) return 1;
    else return Fib(x-2) + Fib(x-1);
}

auto memoized_fib = memoize(Fib);
uint64_t SuperFib(uint64_t x) {
    if (x == 0) return 0;
    else if (x <= 2) return 1;
    else return memoized_fib(x-2) + memoized_fib(x-1);
}

int main(int argc, char *argv[]) {
    int input = atoi(argv[1]);
   //  std::cout << Fib(input) << std::endl;
    std::cout << SuperFib(input) << std::endl;
}

