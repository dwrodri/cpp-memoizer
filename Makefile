CC=/usr/bin/clang
CXX=/usr/bin/clang++
CXXFLAGS=--std=c++17  -Wall

Tests:  *.cpp
	$(CXX) $(CXXFLAGS) $^ -o $@ 

debug: *.cpp
	$(CXX) $(CXXFLAGS) -g $^ -o $@ 


.PHONY: clean
clean:
	rm -rf *.o Tests
