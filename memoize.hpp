#include <functional>
#include <map>
#include <type_traits>
#include <tuple>

template <typename R, typename... Args>
auto __add_cache(std::function<R(Args...)> f) -> std::function<R(Args...)> {
    static auto memoized = [f](Args... args) {
        std::map<std::tuple<Args...>, R> cache;
        if (auto query = cache.find(std::make_tuple(args...)); query == cache.end()) {
            auto result = f(args...);
            cache.insert({std::make_tuple(args...), result});
            return result;
        } else {
            return query->second;
        }
    };
    return memoized;
}


template <typename Callable>
auto memoize(Callable f) -> decltype(std::function(f)) {
    static_assert(std::is_convertible<Callable, decltype(std::function(f))>::value, "You can only memoize functions!");
    return __add_cache(std::function(f));
}
